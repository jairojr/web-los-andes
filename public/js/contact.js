// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyD_WdToEmPG4EYZjPT8c7WmCiazMI7_rKc",
    authDomain: "los-andes-3994a.firebaseapp.com",
    databaseURL: "https://los-andes-3994a.firebaseio.com",
    projectId: "los-andes-3994a",
    storageBucket: "los-andes-3994a.appspot.com",
    messagingSenderId: "880301972752",
    appId: "1:880301972752:web:b3f36601afd5b431add995",
    measurementId: "G-LTNGLM2L64"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const db = firebase.firestore();
//db.settings({ timestampInSnapshots: true});

function enviar() {
    var nombre = document.getElementById("contact-first-name").value;
    var apellido = document.getElementById("contact-last-name").value;
    var email = document.getElementById("contact-email").value;
    var telefono = document.getElementById("contact-phone").value;
    var mensaje = document.getElementById("contact-message").value;

    db.collection("contacts").add({
        nombre: nombre,
        apellido: apellido,
        email: email,
        telefono: telefono,
        mensaje: mensaje
    })
        .then(function(docRef) {
            alert('Su ensaje fue enviado, en breve nos pondremos en contacto.');
            window.location.href= "contacto.html"
        })
        .catch(function(error) {
            alert('Lo sentimos, ha ocurrido un error');
            console.error("Error adding document: ", error);
        });
}